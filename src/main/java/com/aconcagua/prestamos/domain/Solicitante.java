package com.aconcagua.prestamos.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A Solicitante.
 */
@Entity
@Table(name = "solicitante")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Solicitante implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "dni")
    private String dni;

    @Column(name = "name")
    private String name;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "tel_home")
    private String telHome;

    @Column(name = "tel_movile")
    private String telMovile;

    @ManyToOne
    @JsonIgnoreProperties("solicitantes")
    private Prestamo prestamos;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDni() {
        return dni;
    }

    public Solicitante dni(String dni) {
        this.dni = dni;
        return this;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getName() {
        return name;
    }

    public Solicitante name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public Solicitante lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getTelHome() {
        return telHome;
    }

    public Solicitante telHome(String telHome) {
        this.telHome = telHome;
        return this;
    }

    public void setTelHome(String telHome) {
        this.telHome = telHome;
    }

    public String getTelMovile() {
        return telMovile;
    }

    public Solicitante telMovile(String telMovile) {
        this.telMovile = telMovile;
        return this;
    }

    public void setTelMovile(String telMovile) {
        this.telMovile = telMovile;
    }

    public Prestamo getPrestamos() {
        return prestamos;
    }

    public Solicitante prestamos(Prestamo prestamo) {
        this.prestamos = prestamo;
        return this;
    }

    public void setPrestamos(Prestamo prestamo) {
        this.prestamos = prestamo;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Solicitante)) {
            return false;
        }
        return id != null && id.equals(((Solicitante) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Solicitante{" +
            "id=" + getId() +
            ", dni='" + getDni() + "'" +
            ", name='" + getName() + "'" +
            ", lastName='" + getLastName() + "'" +
            ", telHome='" + getTelHome() + "'" +
            ", telMovile='" + getTelMovile() + "'" +
            "}";
    }
}
