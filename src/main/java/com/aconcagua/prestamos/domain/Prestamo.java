package com.aconcagua.prestamos.domain;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A Prestamo.
 */
@Entity
@Table(name = "prestamo")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Prestamo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "nro_prestamo", nullable = false)
    private Long nroPrestamo;

    @NotNull
    @Column(name = "valor", nullable = false)
    private Double valor;

    @Column(name = "cuotas_qty")
    private Integer cuotasQty;

    @Column(name = "date_authorization")
    private LocalDate dateAuthorization;

    @Column(name = "date_delivered")
    private LocalDate dateDelivered;

    @Column(name = "creation_date")
    private LocalDate creationDate;

    @Column(name = "last_updated_date")
    private LocalDate lastUpdatedDate;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNroPrestamo() {
        return nroPrestamo;
    }

    public Prestamo nroPrestamo(Long nroPrestamo) {
        this.nroPrestamo = nroPrestamo;
        return this;
    }

    public void setNroPrestamo(Long nroPrestamo) {
        this.nroPrestamo = nroPrestamo;
    }

    public Double getValor() {
        return valor;
    }

    public Prestamo valor(Double valor) {
        this.valor = valor;
        return this;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Integer getCuotasQty() {
        return cuotasQty;
    }

    public Prestamo cuotasQty(Integer cuotasQty) {
        this.cuotasQty = cuotasQty;
        return this;
    }

    public void setCuotasQty(Integer cuotasQty) {
        this.cuotasQty = cuotasQty;
    }

    public LocalDate getDateAuthorization() {
        return dateAuthorization;
    }

    public Prestamo dateAuthorization(LocalDate dateAuthorization) {
        this.dateAuthorization = dateAuthorization;
        return this;
    }

    public void setDateAuthorization(LocalDate dateAuthorization) {
        this.dateAuthorization = dateAuthorization;
    }

    public LocalDate getDateDelivered() {
        return dateDelivered;
    }

    public Prestamo dateDelivered(LocalDate dateDelivered) {
        this.dateDelivered = dateDelivered;
        return this;
    }

    public void setDateDelivered(LocalDate dateDelivered) {
        this.dateDelivered = dateDelivered;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public Prestamo creationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
        return this;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDate getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    public Prestamo lastUpdatedDate(LocalDate lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
        return this;
    }

    public void setLastUpdatedDate(LocalDate lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Prestamo)) {
            return false;
        }
        return id != null && id.equals(((Prestamo) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Prestamo{" +
            "id=" + getId() +
            ", nroPrestamo=" + getNroPrestamo() +
            ", valor=" + getValor() +
            ", cuotasQty=" + getCuotasQty() +
            ", dateAuthorization='" + getDateAuthorization() + "'" +
            ", dateDelivered='" + getDateDelivered() + "'" +
            ", creationDate='" + getCreationDate() + "'" +
            ", lastUpdatedDate='" + getLastUpdatedDate() + "'" +
            "}";
    }
}
