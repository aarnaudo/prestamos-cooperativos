/**
 * View Models used by Spring MVC REST controllers.
 */
package com.aconcagua.prestamos.web.rest.vm;
