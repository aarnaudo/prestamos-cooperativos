package com.aconcagua.prestamos.web.rest;

import com.aconcagua.prestamos.domain.Solicitante;
import com.aconcagua.prestamos.repository.SolicitanteRepository;
import com.aconcagua.prestamos.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional; 
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.aconcagua.prestamos.domain.Solicitante}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class SolicitanteResource {

    private final Logger log = LoggerFactory.getLogger(SolicitanteResource.class);

    private static final String ENTITY_NAME = "solicitante";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SolicitanteRepository solicitanteRepository;

    public SolicitanteResource(SolicitanteRepository solicitanteRepository) {
        this.solicitanteRepository = solicitanteRepository;
    }

    /**
     * {@code POST  /solicitantes} : Create a new solicitante.
     *
     * @param solicitante the solicitante to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new solicitante, or with status {@code 400 (Bad Request)} if the solicitante has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/solicitantes")
    public ResponseEntity<Solicitante> createSolicitante(@RequestBody Solicitante solicitante) throws URISyntaxException {
        log.debug("REST request to save Solicitante : {}", solicitante);
        if (solicitante.getId() != null) {
            throw new BadRequestAlertException("A new solicitante cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Solicitante result = solicitanteRepository.save(solicitante);
        return ResponseEntity.created(new URI("/api/solicitantes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /solicitantes} : Updates an existing solicitante.
     *
     * @param solicitante the solicitante to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated solicitante,
     * or with status {@code 400 (Bad Request)} if the solicitante is not valid,
     * or with status {@code 500 (Internal Server Error)} if the solicitante couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/solicitantes")
    public ResponseEntity<Solicitante> updateSolicitante(@RequestBody Solicitante solicitante) throws URISyntaxException {
        log.debug("REST request to update Solicitante : {}", solicitante);
        if (solicitante.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Solicitante result = solicitanteRepository.save(solicitante);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, solicitante.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /solicitantes} : get all the solicitantes.
     *

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of solicitantes in body.
     */
    @GetMapping("/solicitantes")
    public List<Solicitante> getAllSolicitantes() {
        log.debug("REST request to get all Solicitantes");
        return solicitanteRepository.findAll();
    }

    /**
     * {@code GET  /solicitantes/:id} : get the "id" solicitante.
     *
     * @param id the id of the solicitante to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the solicitante, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/solicitantes/{id}")
    public ResponseEntity<Solicitante> getSolicitante(@PathVariable Long id) {
        log.debug("REST request to get Solicitante : {}", id);
        Optional<Solicitante> solicitante = solicitanteRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(solicitante);
    }

    /**
     * {@code DELETE  /solicitantes/:id} : delete the "id" solicitante.
     *
     * @param id the id of the solicitante to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/solicitantes/{id}")
    public ResponseEntity<Void> deleteSolicitante(@PathVariable Long id) {
        log.debug("REST request to delete Solicitante : {}", id);
        solicitanteRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
