import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ISolicitante } from 'app/shared/model/solicitante.model';
import { SolicitanteService } from './solicitante.service';
import { SolicitanteDeleteDialogComponent } from './solicitante-delete-dialog.component';

@Component({
  selector: 'jhi-solicitante',
  templateUrl: './solicitante.component.html'
})
export class SolicitanteComponent implements OnInit, OnDestroy {
  solicitantes?: ISolicitante[];
  eventSubscriber?: Subscription;

  constructor(
    protected solicitanteService: SolicitanteService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.solicitanteService.query().subscribe((res: HttpResponse<ISolicitante[]>) => {
      this.solicitantes = res.body ? res.body : [];
    });
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInSolicitantes();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ISolicitante): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInSolicitantes(): void {
    this.eventSubscriber = this.eventManager.subscribe('solicitanteListModification', () => this.loadAll());
  }

  delete(solicitante: ISolicitante): void {
    const modalRef = this.modalService.open(SolicitanteDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.solicitante = solicitante;
  }
}
