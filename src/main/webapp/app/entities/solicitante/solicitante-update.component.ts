import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { ISolicitante, Solicitante } from 'app/shared/model/solicitante.model';
import { SolicitanteService } from './solicitante.service';
import { IPrestamo } from 'app/shared/model/prestamo.model';
import { PrestamoService } from 'app/entities/prestamo/prestamo.service';

@Component({
  selector: 'jhi-solicitante-update',
  templateUrl: './solicitante-update.component.html'
})
export class SolicitanteUpdateComponent implements OnInit {
  isSaving = false;

  prestamos: IPrestamo[] = [];

  editForm = this.fb.group({
    id: [],
    dni: [],
    name: [],
    lastName: [],
    telHome: [],
    telMovile: [],
    prestamos: []
  });

  constructor(
    protected solicitanteService: SolicitanteService,
    protected prestamoService: PrestamoService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ solicitante }) => {
      this.updateForm(solicitante);

      this.prestamoService
        .query()
        .pipe(
          map((res: HttpResponse<IPrestamo[]>) => {
            return res.body ? res.body : [];
          })
        )
        .subscribe((resBody: IPrestamo[]) => (this.prestamos = resBody));
    });
  }

  updateForm(solicitante: ISolicitante): void {
    this.editForm.patchValue({
      id: solicitante.id,
      dni: solicitante.dni,
      name: solicitante.name,
      lastName: solicitante.lastName,
      telHome: solicitante.telHome,
      telMovile: solicitante.telMovile,
      prestamos: solicitante.prestamos
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const solicitante = this.createFromForm();
    if (solicitante.id !== undefined) {
      this.subscribeToSaveResponse(this.solicitanteService.update(solicitante));
    } else {
      this.subscribeToSaveResponse(this.solicitanteService.create(solicitante));
    }
  }

  private createFromForm(): ISolicitante {
    return {
      ...new Solicitante(),
      id: this.editForm.get(['id'])!.value,
      dni: this.editForm.get(['dni'])!.value,
      name: this.editForm.get(['name'])!.value,
      lastName: this.editForm.get(['lastName'])!.value,
      telHome: this.editForm.get(['telHome'])!.value,
      telMovile: this.editForm.get(['telMovile'])!.value,
      prestamos: this.editForm.get(['prestamos'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ISolicitante>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IPrestamo): any {
    return item.id;
  }
}
