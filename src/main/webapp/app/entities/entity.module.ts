import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'prestamo',
        loadChildren: () => import('./prestamo/prestamo.module').then(m => m.PrestamosPrestamoModule)
      },
      {
        path: 'solicitante',
        loadChildren: () => import('./solicitante/solicitante.module').then(m => m.PrestamosSolicitanteModule)
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ]
})
export class PrestamosEntityModule {}
