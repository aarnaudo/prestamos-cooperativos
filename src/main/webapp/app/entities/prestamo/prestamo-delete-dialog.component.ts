import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IPrestamo } from 'app/shared/model/prestamo.model';
import { PrestamoService } from './prestamo.service';

@Component({
  templateUrl: './prestamo-delete-dialog.component.html'
})
export class PrestamoDeleteDialogComponent {
  prestamo?: IPrestamo;

  constructor(protected prestamoService: PrestamoService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.prestamoService.delete(id).subscribe(() => {
      this.eventManager.broadcast('prestamoListModification');
      this.activeModal.close();
    });
  }
}
