import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PrestamosSharedModule } from 'app/shared/shared.module';
import { PrestamoComponent } from './prestamo.component';
import { PrestamoDetailComponent } from './prestamo-detail.component';
import { PrestamoUpdateComponent } from './prestamo-update.component';
import { PrestamoDeleteDialogComponent } from './prestamo-delete-dialog.component';
import { prestamoRoute } from './prestamo.route';

@NgModule({
  imports: [PrestamosSharedModule, RouterModule.forChild(prestamoRoute)],
  declarations: [PrestamoComponent, PrestamoDetailComponent, PrestamoUpdateComponent, PrestamoDeleteDialogComponent],
  entryComponents: [PrestamoDeleteDialogComponent]
})
export class PrestamosPrestamoModule {}
