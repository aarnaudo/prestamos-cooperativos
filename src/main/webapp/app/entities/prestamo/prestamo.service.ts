import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IPrestamo } from 'app/shared/model/prestamo.model';

type EntityResponseType = HttpResponse<IPrestamo>;
type EntityArrayResponseType = HttpResponse<IPrestamo[]>;

@Injectable({ providedIn: 'root' })
export class PrestamoService {
  public resourceUrl = SERVER_API_URL + 'api/prestamos';

  constructor(protected http: HttpClient) {}

  create(prestamo: IPrestamo): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(prestamo);
    return this.http
      .post<IPrestamo>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(prestamo: IPrestamo): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(prestamo);
    return this.http
      .put<IPrestamo>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IPrestamo>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IPrestamo[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(prestamo: IPrestamo): IPrestamo {
    const copy: IPrestamo = Object.assign({}, prestamo, {
      dateAuthorization:
        prestamo.dateAuthorization && prestamo.dateAuthorization.isValid() ? prestamo.dateAuthorization.format(DATE_FORMAT) : undefined,
      dateDelivered: prestamo.dateDelivered && prestamo.dateDelivered.isValid() ? prestamo.dateDelivered.format(DATE_FORMAT) : undefined,
      creationDate: prestamo.creationDate && prestamo.creationDate.isValid() ? prestamo.creationDate.format(DATE_FORMAT) : undefined,
      lastUpdatedDate:
        prestamo.lastUpdatedDate && prestamo.lastUpdatedDate.isValid() ? prestamo.lastUpdatedDate.format(DATE_FORMAT) : undefined
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.dateAuthorization = res.body.dateAuthorization ? moment(res.body.dateAuthorization) : undefined;
      res.body.dateDelivered = res.body.dateDelivered ? moment(res.body.dateDelivered) : undefined;
      res.body.creationDate = res.body.creationDate ? moment(res.body.creationDate) : undefined;
      res.body.lastUpdatedDate = res.body.lastUpdatedDate ? moment(res.body.lastUpdatedDate) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((prestamo: IPrestamo) => {
        prestamo.dateAuthorization = prestamo.dateAuthorization ? moment(prestamo.dateAuthorization) : undefined;
        prestamo.dateDelivered = prestamo.dateDelivered ? moment(prestamo.dateDelivered) : undefined;
        prestamo.creationDate = prestamo.creationDate ? moment(prestamo.creationDate) : undefined;
        prestamo.lastUpdatedDate = prestamo.lastUpdatedDate ? moment(prestamo.lastUpdatedDate) : undefined;
      });
    }
    return res;
  }
}
