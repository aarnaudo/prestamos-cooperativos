import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IPrestamo, Prestamo } from 'app/shared/model/prestamo.model';
import { PrestamoService } from './prestamo.service';
import { PrestamoComponent } from './prestamo.component';
import { PrestamoDetailComponent } from './prestamo-detail.component';
import { PrestamoUpdateComponent } from './prestamo-update.component';

@Injectable({ providedIn: 'root' })
export class PrestamoResolve implements Resolve<IPrestamo> {
  constructor(private service: PrestamoService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IPrestamo> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((prestamo: HttpResponse<Prestamo>) => {
          if (prestamo.body) {
            return of(prestamo.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Prestamo());
  }
}

export const prestamoRoute: Routes = [
  {
    path: '',
    component: PrestamoComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'prestamosApp.prestamo.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: PrestamoDetailComponent,
    resolve: {
      prestamo: PrestamoResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'prestamosApp.prestamo.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: PrestamoUpdateComponent,
    resolve: {
      prestamo: PrestamoResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'prestamosApp.prestamo.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: PrestamoUpdateComponent,
    resolve: {
      prestamo: PrestamoResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'prestamosApp.prestamo.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
