import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';

import { IPrestamo, Prestamo } from 'app/shared/model/prestamo.model';
import { PrestamoService } from './prestamo.service';

@Component({
  selector: 'jhi-prestamo-update',
  templateUrl: './prestamo-update.component.html'
})
export class PrestamoUpdateComponent implements OnInit {
  isSaving = false;
  dateAuthorizationDp: any;
  dateDeliveredDp: any;
  creationDateDp: any;
  lastUpdatedDateDp: any;

  editForm = this.fb.group({
    id: [],
    nroPrestamo: [null, [Validators.required]],
    valor: [null, [Validators.required]],
    cuotasQty: [],
    dateAuthorization: [],
    dateDelivered: [],
    creationDate: [],
    lastUpdatedDate: []
  });

  constructor(protected prestamoService: PrestamoService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ prestamo }) => {
      this.updateForm(prestamo);
    });
  }

  updateForm(prestamo: IPrestamo): void {
    this.editForm.patchValue({
      id: prestamo.id,
      nroPrestamo: prestamo.nroPrestamo,
      valor: prestamo.valor,
      cuotasQty: prestamo.cuotasQty,
      dateAuthorization: prestamo.dateAuthorization,
      dateDelivered: prestamo.dateDelivered,
      creationDate: prestamo.creationDate,
      lastUpdatedDate: prestamo.lastUpdatedDate
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const prestamo = this.createFromForm();
    if (prestamo.id !== undefined) {
      this.subscribeToSaveResponse(this.prestamoService.update(prestamo));
    } else {
      this.subscribeToSaveResponse(this.prestamoService.create(prestamo));
    }
  }

  private createFromForm(): IPrestamo {
    return {
      ...new Prestamo(),
      id: this.editForm.get(['id'])!.value,
      nroPrestamo: this.editForm.get(['nroPrestamo'])!.value,
      valor: this.editForm.get(['valor'])!.value,
      cuotasQty: this.editForm.get(['cuotasQty'])!.value,
      dateAuthorization: this.editForm.get(['dateAuthorization'])!.value,
      dateDelivered: this.editForm.get(['dateDelivered'])!.value,
      creationDate: this.editForm.get(['creationDate'])!.value,
      lastUpdatedDate: this.editForm.get(['lastUpdatedDate'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPrestamo>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
