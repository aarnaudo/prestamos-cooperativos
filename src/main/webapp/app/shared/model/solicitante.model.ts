import { IPrestamo } from 'app/shared/model/prestamo.model';

export interface ISolicitante {
  id?: number;
  dni?: string;
  name?: string;
  lastName?: string;
  telHome?: string;
  telMovile?: string;
  prestamos?: IPrestamo;
}

export class Solicitante implements ISolicitante {
  constructor(
    public id?: number,
    public dni?: string,
    public name?: string,
    public lastName?: string,
    public telHome?: string,
    public telMovile?: string,
    public prestamos?: IPrestamo
  ) {}
}
