import { Moment } from 'moment';

export interface IPrestamo {
  id?: number;
  nroPrestamo?: number;
  valor?: number;
  cuotasQty?: number;
  dateAuthorization?: Moment;
  dateDelivered?: Moment;
  creationDate?: Moment;
  lastUpdatedDate?: Moment;
}

export class Prestamo implements IPrestamo {
  constructor(
    public id?: number,
    public nroPrestamo?: number,
    public valor?: number,
    public cuotasQty?: number,
    public dateAuthorization?: Moment,
    public dateDelivered?: Moment,
    public creationDate?: Moment,
    public lastUpdatedDate?: Moment
  ) {}
}
