package com.aconcagua.prestamos.web.rest;

import com.aconcagua.prestamos.PrestamosApp;
import com.aconcagua.prestamos.domain.Prestamo;
import com.aconcagua.prestamos.repository.PrestamoRepository;
import com.aconcagua.prestamos.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.aconcagua.prestamos.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link PrestamoResource} REST controller.
 */
@SpringBootTest(classes = PrestamosApp.class)
public class PrestamoResourceIT {

    private static final Long DEFAULT_NRO_PRESTAMO = 1L;
    private static final Long UPDATED_NRO_PRESTAMO = 2L;

    private static final Double DEFAULT_VALOR = 1D;
    private static final Double UPDATED_VALOR = 2D;

    private static final Integer DEFAULT_CUOTAS_QTY = 1;
    private static final Integer UPDATED_CUOTAS_QTY = 2;

    private static final LocalDate DEFAULT_DATE_AUTHORIZATION = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_AUTHORIZATION = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_DATE_DELIVERED = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_DELIVERED = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_CREATION_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATION_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_LAST_UPDATED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_UPDATED_DATE = LocalDate.now(ZoneId.systemDefault());

    @Autowired
    private PrestamoRepository prestamoRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restPrestamoMockMvc;

    private Prestamo prestamo;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PrestamoResource prestamoResource = new PrestamoResource(prestamoRepository);
        this.restPrestamoMockMvc = MockMvcBuilders.standaloneSetup(prestamoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Prestamo createEntity(EntityManager em) {
        Prestamo prestamo = new Prestamo()
            .nroPrestamo(DEFAULT_NRO_PRESTAMO)
            .valor(DEFAULT_VALOR)
            .cuotasQty(DEFAULT_CUOTAS_QTY)
            .dateAuthorization(DEFAULT_DATE_AUTHORIZATION)
            .dateDelivered(DEFAULT_DATE_DELIVERED)
            .creationDate(DEFAULT_CREATION_DATE)
            .lastUpdatedDate(DEFAULT_LAST_UPDATED_DATE);
        return prestamo;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Prestamo createUpdatedEntity(EntityManager em) {
        Prestamo prestamo = new Prestamo()
            .nroPrestamo(UPDATED_NRO_PRESTAMO)
            .valor(UPDATED_VALOR)
            .cuotasQty(UPDATED_CUOTAS_QTY)
            .dateAuthorization(UPDATED_DATE_AUTHORIZATION)
            .dateDelivered(UPDATED_DATE_DELIVERED)
            .creationDate(UPDATED_CREATION_DATE)
            .lastUpdatedDate(UPDATED_LAST_UPDATED_DATE);
        return prestamo;
    }

    @BeforeEach
    public void initTest() {
        prestamo = createEntity(em);
    }

    @Test
    @Transactional
    public void createPrestamo() throws Exception {
        int databaseSizeBeforeCreate = prestamoRepository.findAll().size();

        // Create the Prestamo
        restPrestamoMockMvc.perform(post("/api/prestamos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(prestamo)))
            .andExpect(status().isCreated());

        // Validate the Prestamo in the database
        List<Prestamo> prestamoList = prestamoRepository.findAll();
        assertThat(prestamoList).hasSize(databaseSizeBeforeCreate + 1);
        Prestamo testPrestamo = prestamoList.get(prestamoList.size() - 1);
        assertThat(testPrestamo.getNroPrestamo()).isEqualTo(DEFAULT_NRO_PRESTAMO);
        assertThat(testPrestamo.getValor()).isEqualTo(DEFAULT_VALOR);
        assertThat(testPrestamo.getCuotasQty()).isEqualTo(DEFAULT_CUOTAS_QTY);
        assertThat(testPrestamo.getDateAuthorization()).isEqualTo(DEFAULT_DATE_AUTHORIZATION);
        assertThat(testPrestamo.getDateDelivered()).isEqualTo(DEFAULT_DATE_DELIVERED);
        assertThat(testPrestamo.getCreationDate()).isEqualTo(DEFAULT_CREATION_DATE);
        assertThat(testPrestamo.getLastUpdatedDate()).isEqualTo(DEFAULT_LAST_UPDATED_DATE);
    }

    @Test
    @Transactional
    public void createPrestamoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = prestamoRepository.findAll().size();

        // Create the Prestamo with an existing ID
        prestamo.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPrestamoMockMvc.perform(post("/api/prestamos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(prestamo)))
            .andExpect(status().isBadRequest());

        // Validate the Prestamo in the database
        List<Prestamo> prestamoList = prestamoRepository.findAll();
        assertThat(prestamoList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkNroPrestamoIsRequired() throws Exception {
        int databaseSizeBeforeTest = prestamoRepository.findAll().size();
        // set the field null
        prestamo.setNroPrestamo(null);

        // Create the Prestamo, which fails.

        restPrestamoMockMvc.perform(post("/api/prestamos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(prestamo)))
            .andExpect(status().isBadRequest());

        List<Prestamo> prestamoList = prestamoRepository.findAll();
        assertThat(prestamoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkValorIsRequired() throws Exception {
        int databaseSizeBeforeTest = prestamoRepository.findAll().size();
        // set the field null
        prestamo.setValor(null);

        // Create the Prestamo, which fails.

        restPrestamoMockMvc.perform(post("/api/prestamos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(prestamo)))
            .andExpect(status().isBadRequest());

        List<Prestamo> prestamoList = prestamoRepository.findAll();
        assertThat(prestamoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPrestamos() throws Exception {
        // Initialize the database
        prestamoRepository.saveAndFlush(prestamo);

        // Get all the prestamoList
        restPrestamoMockMvc.perform(get("/api/prestamos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(prestamo.getId().intValue())))
            .andExpect(jsonPath("$.[*].nroPrestamo").value(hasItem(DEFAULT_NRO_PRESTAMO.intValue())))
            .andExpect(jsonPath("$.[*].valor").value(hasItem(DEFAULT_VALOR.doubleValue())))
            .andExpect(jsonPath("$.[*].cuotasQty").value(hasItem(DEFAULT_CUOTAS_QTY)))
            .andExpect(jsonPath("$.[*].dateAuthorization").value(hasItem(DEFAULT_DATE_AUTHORIZATION.toString())))
            .andExpect(jsonPath("$.[*].dateDelivered").value(hasItem(DEFAULT_DATE_DELIVERED.toString())))
            .andExpect(jsonPath("$.[*].creationDate").value(hasItem(DEFAULT_CREATION_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastUpdatedDate").value(hasItem(DEFAULT_LAST_UPDATED_DATE.toString())));
    }
    
    @Test
    @Transactional
    public void getPrestamo() throws Exception {
        // Initialize the database
        prestamoRepository.saveAndFlush(prestamo);

        // Get the prestamo
        restPrestamoMockMvc.perform(get("/api/prestamos/{id}", prestamo.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(prestamo.getId().intValue()))
            .andExpect(jsonPath("$.nroPrestamo").value(DEFAULT_NRO_PRESTAMO.intValue()))
            .andExpect(jsonPath("$.valor").value(DEFAULT_VALOR.doubleValue()))
            .andExpect(jsonPath("$.cuotasQty").value(DEFAULT_CUOTAS_QTY))
            .andExpect(jsonPath("$.dateAuthorization").value(DEFAULT_DATE_AUTHORIZATION.toString()))
            .andExpect(jsonPath("$.dateDelivered").value(DEFAULT_DATE_DELIVERED.toString()))
            .andExpect(jsonPath("$.creationDate").value(DEFAULT_CREATION_DATE.toString()))
            .andExpect(jsonPath("$.lastUpdatedDate").value(DEFAULT_LAST_UPDATED_DATE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPrestamo() throws Exception {
        // Get the prestamo
        restPrestamoMockMvc.perform(get("/api/prestamos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePrestamo() throws Exception {
        // Initialize the database
        prestamoRepository.saveAndFlush(prestamo);

        int databaseSizeBeforeUpdate = prestamoRepository.findAll().size();

        // Update the prestamo
        Prestamo updatedPrestamo = prestamoRepository.findById(prestamo.getId()).get();
        // Disconnect from session so that the updates on updatedPrestamo are not directly saved in db
        em.detach(updatedPrestamo);
        updatedPrestamo
            .nroPrestamo(UPDATED_NRO_PRESTAMO)
            .valor(UPDATED_VALOR)
            .cuotasQty(UPDATED_CUOTAS_QTY)
            .dateAuthorization(UPDATED_DATE_AUTHORIZATION)
            .dateDelivered(UPDATED_DATE_DELIVERED)
            .creationDate(UPDATED_CREATION_DATE)
            .lastUpdatedDate(UPDATED_LAST_UPDATED_DATE);

        restPrestamoMockMvc.perform(put("/api/prestamos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedPrestamo)))
            .andExpect(status().isOk());

        // Validate the Prestamo in the database
        List<Prestamo> prestamoList = prestamoRepository.findAll();
        assertThat(prestamoList).hasSize(databaseSizeBeforeUpdate);
        Prestamo testPrestamo = prestamoList.get(prestamoList.size() - 1);
        assertThat(testPrestamo.getNroPrestamo()).isEqualTo(UPDATED_NRO_PRESTAMO);
        assertThat(testPrestamo.getValor()).isEqualTo(UPDATED_VALOR);
        assertThat(testPrestamo.getCuotasQty()).isEqualTo(UPDATED_CUOTAS_QTY);
        assertThat(testPrestamo.getDateAuthorization()).isEqualTo(UPDATED_DATE_AUTHORIZATION);
        assertThat(testPrestamo.getDateDelivered()).isEqualTo(UPDATED_DATE_DELIVERED);
        assertThat(testPrestamo.getCreationDate()).isEqualTo(UPDATED_CREATION_DATE);
        assertThat(testPrestamo.getLastUpdatedDate()).isEqualTo(UPDATED_LAST_UPDATED_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingPrestamo() throws Exception {
        int databaseSizeBeforeUpdate = prestamoRepository.findAll().size();

        // Create the Prestamo

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPrestamoMockMvc.perform(put("/api/prestamos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(prestamo)))
            .andExpect(status().isBadRequest());

        // Validate the Prestamo in the database
        List<Prestamo> prestamoList = prestamoRepository.findAll();
        assertThat(prestamoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePrestamo() throws Exception {
        // Initialize the database
        prestamoRepository.saveAndFlush(prestamo);

        int databaseSizeBeforeDelete = prestamoRepository.findAll().size();

        // Delete the prestamo
        restPrestamoMockMvc.perform(delete("/api/prestamos/{id}", prestamo.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Prestamo> prestamoList = prestamoRepository.findAll();
        assertThat(prestamoList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
