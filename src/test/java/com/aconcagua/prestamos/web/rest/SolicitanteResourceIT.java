package com.aconcagua.prestamos.web.rest;

import com.aconcagua.prestamos.PrestamosApp;
import com.aconcagua.prestamos.domain.Solicitante;
import com.aconcagua.prestamos.repository.SolicitanteRepository;
import com.aconcagua.prestamos.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.aconcagua.prestamos.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link SolicitanteResource} REST controller.
 */
@SpringBootTest(classes = PrestamosApp.class)
public class SolicitanteResourceIT {

    private static final String DEFAULT_DNI = "AAAAAAAAAA";
    private static final String UPDATED_DNI = "BBBBBBBBBB";

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_LAST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_LAST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_TEL_HOME = "AAAAAAAAAA";
    private static final String UPDATED_TEL_HOME = "BBBBBBBBBB";

    private static final String DEFAULT_TEL_MOVILE = "AAAAAAAAAA";
    private static final String UPDATED_TEL_MOVILE = "BBBBBBBBBB";

    @Autowired
    private SolicitanteRepository solicitanteRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restSolicitanteMockMvc;

    private Solicitante solicitante;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SolicitanteResource solicitanteResource = new SolicitanteResource(solicitanteRepository);
        this.restSolicitanteMockMvc = MockMvcBuilders.standaloneSetup(solicitanteResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Solicitante createEntity(EntityManager em) {
        Solicitante solicitante = new Solicitante()
            .dni(DEFAULT_DNI)
            .name(DEFAULT_NAME)
            .lastName(DEFAULT_LAST_NAME)
            .telHome(DEFAULT_TEL_HOME)
            .telMovile(DEFAULT_TEL_MOVILE);
        return solicitante;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Solicitante createUpdatedEntity(EntityManager em) {
        Solicitante solicitante = new Solicitante()
            .dni(UPDATED_DNI)
            .name(UPDATED_NAME)
            .lastName(UPDATED_LAST_NAME)
            .telHome(UPDATED_TEL_HOME)
            .telMovile(UPDATED_TEL_MOVILE);
        return solicitante;
    }

    @BeforeEach
    public void initTest() {
        solicitante = createEntity(em);
    }

    @Test
    @Transactional
    public void createSolicitante() throws Exception {
        int databaseSizeBeforeCreate = solicitanteRepository.findAll().size();

        // Create the Solicitante
        restSolicitanteMockMvc.perform(post("/api/solicitantes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(solicitante)))
            .andExpect(status().isCreated());

        // Validate the Solicitante in the database
        List<Solicitante> solicitanteList = solicitanteRepository.findAll();
        assertThat(solicitanteList).hasSize(databaseSizeBeforeCreate + 1);
        Solicitante testSolicitante = solicitanteList.get(solicitanteList.size() - 1);
        assertThat(testSolicitante.getDni()).isEqualTo(DEFAULT_DNI);
        assertThat(testSolicitante.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testSolicitante.getLastName()).isEqualTo(DEFAULT_LAST_NAME);
        assertThat(testSolicitante.getTelHome()).isEqualTo(DEFAULT_TEL_HOME);
        assertThat(testSolicitante.getTelMovile()).isEqualTo(DEFAULT_TEL_MOVILE);
    }

    @Test
    @Transactional
    public void createSolicitanteWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = solicitanteRepository.findAll().size();

        // Create the Solicitante with an existing ID
        solicitante.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSolicitanteMockMvc.perform(post("/api/solicitantes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(solicitante)))
            .andExpect(status().isBadRequest());

        // Validate the Solicitante in the database
        List<Solicitante> solicitanteList = solicitanteRepository.findAll();
        assertThat(solicitanteList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllSolicitantes() throws Exception {
        // Initialize the database
        solicitanteRepository.saveAndFlush(solicitante);

        // Get all the solicitanteList
        restSolicitanteMockMvc.perform(get("/api/solicitantes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(solicitante.getId().intValue())))
            .andExpect(jsonPath("$.[*].dni").value(hasItem(DEFAULT_DNI)))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LAST_NAME)))
            .andExpect(jsonPath("$.[*].telHome").value(hasItem(DEFAULT_TEL_HOME)))
            .andExpect(jsonPath("$.[*].telMovile").value(hasItem(DEFAULT_TEL_MOVILE)));
    }
    
    @Test
    @Transactional
    public void getSolicitante() throws Exception {
        // Initialize the database
        solicitanteRepository.saveAndFlush(solicitante);

        // Get the solicitante
        restSolicitanteMockMvc.perform(get("/api/solicitantes/{id}", solicitante.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(solicitante.getId().intValue()))
            .andExpect(jsonPath("$.dni").value(DEFAULT_DNI))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.lastName").value(DEFAULT_LAST_NAME))
            .andExpect(jsonPath("$.telHome").value(DEFAULT_TEL_HOME))
            .andExpect(jsonPath("$.telMovile").value(DEFAULT_TEL_MOVILE));
    }

    @Test
    @Transactional
    public void getNonExistingSolicitante() throws Exception {
        // Get the solicitante
        restSolicitanteMockMvc.perform(get("/api/solicitantes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSolicitante() throws Exception {
        // Initialize the database
        solicitanteRepository.saveAndFlush(solicitante);

        int databaseSizeBeforeUpdate = solicitanteRepository.findAll().size();

        // Update the solicitante
        Solicitante updatedSolicitante = solicitanteRepository.findById(solicitante.getId()).get();
        // Disconnect from session so that the updates on updatedSolicitante are not directly saved in db
        em.detach(updatedSolicitante);
        updatedSolicitante
            .dni(UPDATED_DNI)
            .name(UPDATED_NAME)
            .lastName(UPDATED_LAST_NAME)
            .telHome(UPDATED_TEL_HOME)
            .telMovile(UPDATED_TEL_MOVILE);

        restSolicitanteMockMvc.perform(put("/api/solicitantes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedSolicitante)))
            .andExpect(status().isOk());

        // Validate the Solicitante in the database
        List<Solicitante> solicitanteList = solicitanteRepository.findAll();
        assertThat(solicitanteList).hasSize(databaseSizeBeforeUpdate);
        Solicitante testSolicitante = solicitanteList.get(solicitanteList.size() - 1);
        assertThat(testSolicitante.getDni()).isEqualTo(UPDATED_DNI);
        assertThat(testSolicitante.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testSolicitante.getLastName()).isEqualTo(UPDATED_LAST_NAME);
        assertThat(testSolicitante.getTelHome()).isEqualTo(UPDATED_TEL_HOME);
        assertThat(testSolicitante.getTelMovile()).isEqualTo(UPDATED_TEL_MOVILE);
    }

    @Test
    @Transactional
    public void updateNonExistingSolicitante() throws Exception {
        int databaseSizeBeforeUpdate = solicitanteRepository.findAll().size();

        // Create the Solicitante

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSolicitanteMockMvc.perform(put("/api/solicitantes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(solicitante)))
            .andExpect(status().isBadRequest());

        // Validate the Solicitante in the database
        List<Solicitante> solicitanteList = solicitanteRepository.findAll();
        assertThat(solicitanteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSolicitante() throws Exception {
        // Initialize the database
        solicitanteRepository.saveAndFlush(solicitante);

        int databaseSizeBeforeDelete = solicitanteRepository.findAll().size();

        // Delete the solicitante
        restSolicitanteMockMvc.perform(delete("/api/solicitantes/{id}", solicitante.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Solicitante> solicitanteList = solicitanteRepository.findAll();
        assertThat(solicitanteList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
