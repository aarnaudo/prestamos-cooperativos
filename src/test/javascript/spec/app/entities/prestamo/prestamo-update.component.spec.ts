import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { PrestamosTestModule } from '../../../test.module';
import { PrestamoUpdateComponent } from 'app/entities/prestamo/prestamo-update.component';
import { PrestamoService } from 'app/entities/prestamo/prestamo.service';
import { Prestamo } from 'app/shared/model/prestamo.model';

describe('Component Tests', () => {
  describe('Prestamo Management Update Component', () => {
    let comp: PrestamoUpdateComponent;
    let fixture: ComponentFixture<PrestamoUpdateComponent>;
    let service: PrestamoService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PrestamosTestModule],
        declarations: [PrestamoUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(PrestamoUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(PrestamoUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(PrestamoService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Prestamo(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Prestamo();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
