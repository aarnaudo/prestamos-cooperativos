import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { take, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { PrestamoService } from 'app/entities/prestamo/prestamo.service';
import { IPrestamo, Prestamo } from 'app/shared/model/prestamo.model';

describe('Service Tests', () => {
  describe('Prestamo Service', () => {
    let injector: TestBed;
    let service: PrestamoService;
    let httpMock: HttpTestingController;
    let elemDefault: IPrestamo;
    let expectedResult: IPrestamo | IPrestamo[] | boolean | null;
    let currentDate: moment.Moment;
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(PrestamoService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new Prestamo(0, 0, 0, 0, currentDate, currentDate, currentDate, currentDate);
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            dateAuthorization: currentDate.format(DATE_FORMAT),
            dateDelivered: currentDate.format(DATE_FORMAT),
            creationDate: currentDate.format(DATE_FORMAT),
            lastUpdatedDate: currentDate.format(DATE_FORMAT)
          },
          elemDefault
        );
        service
          .find(123)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a Prestamo', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            dateAuthorization: currentDate.format(DATE_FORMAT),
            dateDelivered: currentDate.format(DATE_FORMAT),
            creationDate: currentDate.format(DATE_FORMAT),
            lastUpdatedDate: currentDate.format(DATE_FORMAT)
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            dateAuthorization: currentDate,
            dateDelivered: currentDate,
            creationDate: currentDate,
            lastUpdatedDate: currentDate
          },
          returnedFromService
        );
        service
          .create(new Prestamo())
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp.body));
        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a Prestamo', () => {
        const returnedFromService = Object.assign(
          {
            nroPrestamo: 1,
            valor: 1,
            cuotasQty: 1,
            dateAuthorization: currentDate.format(DATE_FORMAT),
            dateDelivered: currentDate.format(DATE_FORMAT),
            creationDate: currentDate.format(DATE_FORMAT),
            lastUpdatedDate: currentDate.format(DATE_FORMAT)
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            dateAuthorization: currentDate,
            dateDelivered: currentDate,
            creationDate: currentDate,
            lastUpdatedDate: currentDate
          },
          returnedFromService
        );
        service
          .update(expected)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp.body));
        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of Prestamo', () => {
        const returnedFromService = Object.assign(
          {
            nroPrestamo: 1,
            valor: 1,
            cuotasQty: 1,
            dateAuthorization: currentDate.format(DATE_FORMAT),
            dateDelivered: currentDate.format(DATE_FORMAT),
            creationDate: currentDate.format(DATE_FORMAT),
            lastUpdatedDate: currentDate.format(DATE_FORMAT)
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            dateAuthorization: currentDate,
            dateDelivered: currentDate,
            creationDate: currentDate,
            lastUpdatedDate: currentDate
          },
          returnedFromService
        );
        service
          .query()
          .pipe(
            take(1),
            map(resp => resp.body)
          )
          .subscribe(body => (expectedResult = body));
        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Prestamo', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
