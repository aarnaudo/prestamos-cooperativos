import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PrestamosTestModule } from '../../../test.module';
import { PrestamoDetailComponent } from 'app/entities/prestamo/prestamo-detail.component';
import { Prestamo } from 'app/shared/model/prestamo.model';

describe('Component Tests', () => {
  describe('Prestamo Management Detail Component', () => {
    let comp: PrestamoDetailComponent;
    let fixture: ComponentFixture<PrestamoDetailComponent>;
    const route = ({ data: of({ prestamo: new Prestamo(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PrestamosTestModule],
        declarations: [PrestamoDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(PrestamoDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(PrestamoDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load prestamo on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.prestamo).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
