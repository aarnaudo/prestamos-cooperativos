import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { PrestamosTestModule } from '../../../test.module';
import { SolicitanteComponent } from 'app/entities/solicitante/solicitante.component';
import { SolicitanteService } from 'app/entities/solicitante/solicitante.service';
import { Solicitante } from 'app/shared/model/solicitante.model';

describe('Component Tests', () => {
  describe('Solicitante Management Component', () => {
    let comp: SolicitanteComponent;
    let fixture: ComponentFixture<SolicitanteComponent>;
    let service: SolicitanteService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PrestamosTestModule],
        declarations: [SolicitanteComponent],
        providers: []
      })
        .overrideTemplate(SolicitanteComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(SolicitanteComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(SolicitanteService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Solicitante(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.solicitantes && comp.solicitantes[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
